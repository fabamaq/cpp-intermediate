#include "gtest/gtest.h"

#include <iostream>
#include <numeric>

/******************************************************************************
 * Helpers
 *****************************************************************************/
void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

void BubbleSort(int *A, int length) {
	for (int i = 0; i < length - 1 - 1; i++) {
		for (int j = length - 1; j >= i + 1; j--) {
			if (A[j] < A[j - 1])
				std::swap(A[j], A[j - 1]);
		}
	};
}

TEST(BubbleSortTestSuite, TestBubbleSort) {
	int numbers[]{5, 2, 4, 7, 1, 3, 2, 6};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	BubbleSort(numbers, numbers_size);
	int expected[]{1, 2, 2, 3, 4, 5, 6, 7};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}
