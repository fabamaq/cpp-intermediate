#include "gtest/gtest.h"

#include <algorithm>
#include <iostream>

/******************************************************************************
 * Helpers
 *****************************************************************************/
void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

/******************************************************************************
 * Heaps
 *****************************************************************************/
inline int Parent(int i) { return i / 2; }
inline int Left(int i) { return 2 * i; }
inline int Right(int i) { return 2 * i + 1; }

/******************************************************************************
 * Maintaining the heap property
 *****************************************************************************/
void MaxHeapify(int *A, int heapSize, int i) {
	int largest = i;
	int l = Left(i);
	int r = Right(i);
	if (l <= heapSize - 1 && A[l] > A[i]) {
		largest = l;
	} else {
		largest = i;
	}
	if (r <= heapSize - 1 && A[r] > A[largest]) {
		largest = r;
	}
	if (largest != i) {
		std::swap(A[i], A[largest]);
		MaxHeapify(A, heapSize, largest);
	}
}

/******************************************************************************
 * Building a heap
 *****************************************************************************/
void BuildMaxHeap(int *A, int size) {
	for (int i = size / 2 - 1; i >= 0; i--) {
		MaxHeapify(A, size, i);
	}
}

/******************************************************************************
 * The heapsort algorithm
 *****************************************************************************/
void HeapSort(int *A, int size) {
	BuildMaxHeap(A, size);
	for (int i = size - 1; i > 0; i--) {
		std::swap(A[0], A[i]);
		MaxHeapify(A, i, 0);
	}
}

TEST(HeapSortTestSuite, TestHeapSort) {
	int numbers[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	HeapSort(numbers, numbers_size);
	int expected[]{1, 2, 3, 4, 7, 8, 9, 10, 14, 16};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}
