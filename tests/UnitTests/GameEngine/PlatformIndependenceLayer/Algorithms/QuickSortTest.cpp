#include "gtest/gtest.h"

#include <algorithm>
#include <iostream>

/******************************************************************************
 * Helpers
 *****************************************************************************/
void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

/******************************************************************************
 * Description of quicksort
 *****************************************************************************/
int Partition(int *A, int p, int r) {
	auto x = A[r];
	auto i = p - 1;
	for (auto j = p; j < r; j++) {
		if (A[j] <= x) {
			i = i + 1;
			std::swap(A[i], A[j]);
		}
	}
	std::swap(A[i + 1], A[r]);
	return i + 1;
}

void QuickSort(int *A, int p, int r) {
	if (p < r) {
		auto q = Partition(A, p, r);
		QuickSort(A, p, q - 1);
		QuickSort(A, q + 1, r);
	}
}

TEST(QuickSortTestSuite, TestQuickSort) {
	int numbers[]{2, 8, 7, 1, 3, 5, 6, 4};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	QuickSort(numbers, 0, numbers_size - 1);
	int expected[]{1, 2, 3, 4, 5, 6, 7, 8};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}
