#include "gtest/gtest.h"

#include <iostream>
#include <numeric>

/******************************************************************************
 * Helpers
 *****************************************************************************/
void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

/******************************************************************************
 * InsertionSortAsc
 *****************************************************************************/
void InsertionSortAsc(int *A, int length) {
	// for j = 2 to A.length
	for (int j = 1; j < length; j++) {
		// key = A[j]
		auto key = A[j];
		// Insert A[j] into the sorted sequence A[1 .. j - 1].
		auto i = j - 1;
		// while i > 0 and A[i] > key
		while (i >= 0 && A[i] > key) {
			A[i + 1] = A[i];
			i = i - 1;
		}
		A[i + 1] = key;
	}
}

TEST(InsertionSortTestSuite, TestInsertionSortAsc) {
	int numbers[]{5, 2, 4, 6, 1, 3};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	InsertionSortAsc(numbers, numbers_size);
	int expected[]{1, 2, 3, 4, 5, 6};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}

TEST(InsertionSortTestSuite, TestInsertionSortAsc2) {
	int numbers[]{31, 41, 59, 26, 41, 58};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	InsertionSortAsc(numbers, numbers_size);
	int expected[]{26, 31, 41, 41, 58, 59};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}

/******************************************************************************
 * InsertionSortDesc
 *****************************************************************************/
void InsertionSortDesc(int *A, int length) {
	// for j = A.lentgh - 1 to 0
	for (int j = length - 2; j >= 0; j--) {
		// key = A[j]
		auto key = A[j];
		// Insert A[j] into the sorted sequence A[j + 1 .. length].
		auto i = j + 1;
		// while i > 0 and A[i] > key
		while (i < length && A[i] > key) {
			A[i - 1] = A[i];
			i = i + 1;
		}
		A[i - 1] = key;
	}
}

TEST(InsertionSortTestSuite, TestInsertionSortDesc) {
	int numbers[]{5, 2, 4, 6, 1, 3};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	InsertionSortDesc(numbers, numbers_size);
	int expected[]{6, 5, 4, 3, 2, 1};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}
