#include "gtest/gtest.h"

#include <algorithm>
#include <chrono>
#include <numeric>
#include <random>
#include <vector>

/// ===========================================================================
/// @section Helper Functions
/// ===========================================================================

void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

constexpr std::size_t cxMaxSize = 1000;

inline std::vector<int> GetData() {
	static std::vector<int> result;
	if (result.empty()) {
		std::default_random_engine dre;
		result.resize(cxMaxSize);
		std::iota(result.begin(), result.end(), 1);
		std::shuffle(result.begin(), result.end(), dre);
	}
	return result; // copy
}

/// ===========================================================================
/// @section Sorting Algorithms
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection BubbleSort
/// ---------------------------------------------------------------------------

void BubbleSort(int *A, int length) {
	for (int i = 0; i < length - 1 - 1; i++) {
		for (int j = length - 1; j >= i + 1; j--) {
			if (A[j] < A[j - 1])
				std::swap(A[j], A[j - 1]);
		}
	};
}

TEST(BenchmarkTestSuite, BenchmarkBubbleSort) {
	auto numbers = GetData();
	auto length = static_cast<int>(numbers.size());

	auto start = std::chrono::high_resolution_clock::now();
	BubbleSort(numbers.data(), length);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	// PrintData(numbers.data(), length, "BubbleSort");
	printf("Test took %f seconds\n", diff.count());
}

/// ---------------------------------------------------------------------------
/// @subsection CountingSort
/// ---------------------------------------------------------------------------

void CountingSort(int *A, int length, int *B, int k) {
	int *C = new int[k + 1];
	for (int i = 0; i <= k; i++) {
		C[i] = 0;
	}
	for (int j = 0; j < length; j++) {
		C[A[j]] = C[A[j]] + 1;
	}
	// C[i] now contains the number of elements equal to i.
	for (int i = 1; i <= k; i++) {
		C[i] = C[i] + C[i - 1];
	}
	// C[i] now contains the number of elements less than or equal to i.
	for (int j = length - 1; j >= 0; j--) {
		B[C[A[j]] - 1] = A[j];
		C[A[j]] = C[A[j]] - 1;
	}
	delete[] C;
}

TEST(BenchmarkTestSuite, BenchmarkCountingSort) {
	auto numbers = GetData();
	const auto length = static_cast<int>(numbers.size());
	std::vector<int> result(numbers.size());

	auto start = std::chrono::high_resolution_clock::now();
	auto k = *std::max_element(numbers.begin(), numbers.end());
	CountingSort(numbers.data(), length, result.data(), k);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	// PrintData(result.data(), length, "CountingSort");
	printf("Test took %f seconds\n", diff.count());
}

/// ---------------------------------------------------------------------------
/// @subsection MergeSort
/// ---------------------------------------------------------------------------

void Merge(int *A, int p, int q, int r) {
	auto n1 = static_cast<unsigned long>(q - p + 1);
	auto n2 = static_cast<unsigned long>(r - q);
	int *L = new int[n1 + 1];
	int *R = new int[n2 + 1];
	for (unsigned long i = 0; i < n1; i++)
		L[i] = A[static_cast<unsigned long>(p) + i - 1];
	for (unsigned long j = 0; j < n2; j++)
		R[j] = A[static_cast<unsigned long>(q) + j];
	L[n1] = std::numeric_limits<int>::max();
	R[n2] = std::numeric_limits<int>::max();
	int i = 0;
	int j = 0;
	for (int k = p - 1; k < r; k++) {
		if (L[i] <= R[j]) {
			A[k] = L[i];
			i = i + 1;
		} else {
			A[k] = R[j];
			j = j + 1;
		}
	};
	delete[] L;
	delete[] R;
}

// [ r1, r2, ..., r n/2, ..., rn]
void MergeSort(int *A, int p, int r) {
	if (p < r) {
		int q = (p + r) / 2;
		MergeSort(A, p, q);
		MergeSort(A, q + 1, r);
		Merge(A, p, q, r);
	}
}

TEST(BenchmarkTestSuite, BenchmarkMergeSort) {
	auto numbers = GetData();
	const auto length = static_cast<int>(numbers.size());

	auto start = std::chrono::high_resolution_clock::now();
	MergeSort(numbers.data(), 1, length);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	// PrintData(numbers.data(), length, "MergeSort");
	printf("Test took %f seconds\n", diff.count());
}

/// ---------------------------------------------------------------------------
/// @subsection HeapSort
/// ---------------------------------------------------------------------------

/// Heaps
inline int Parent(int i) { return i / 2; }
inline int Left(int i) { return 2 * i; }
inline int Right(int i) { return 2 * i + 1; }

/// Maintaining the heap property
void MaxHeapify(int *A, int heapSize, int i) {
	int largest = i;
	int l = Left(i);
	int r = Right(i);
	if (l <= heapSize - 1 && A[l] > A[i]) {
		largest = l;
	} else {
		largest = i;
	}
	if (r <= heapSize - 1 && A[r] > A[largest]) {
		largest = r;
	}
	if (largest != i) {
		std::swap(A[i], A[largest]);
		MaxHeapify(A, heapSize, largest);
	}
}

/// Building a heap
void BuildMaxHeap(int *A, int size) {
	for (int i = size / 2 - 1; i >= 0; i--) {
		MaxHeapify(A, size, i);
	}
}

/// The heapsort algorithm
void HeapSort(int *A, int size) {
	BuildMaxHeap(A, size);
	for (int i = size - 1; i > 0; i--) {
		std::swap(A[0], A[i]);
		MaxHeapify(A, i, 0);
	}
}

TEST(BenchmarkTestSuite, BenchmarkHeapSort) {
	auto numbers = GetData();
	const auto length = static_cast<int>(numbers.size());

	auto start = std::chrono::high_resolution_clock::now();
	HeapSort(numbers.data(), length);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	// PrintData(numbers.data(), length, "HeapSort");
	printf("Test took %f seconds\n", diff.count());
}

/// ---------------------------------------------------------------------------
/// @subsection InsertionSort
/// ---------------------------------------------------------------------------

void InsertionSort(int *A, int length) {
	// for j = 2 to A.length
	for (int j = 1; j < length; j++) {
		// key = A[j]
		auto key = A[j];
		// Insert A[j] into the sorted sequence A[1 .. j - 1].
		auto i = j - 1;
		// while i > 0 and A[i] > key
		while (i >= 0 && A[i] > key) {
			A[i + 1] = A[i];
			i = i - 1;
		}
		A[i + 1] = key;
	}
}

TEST(BenchmarkTestSuite, BenchmarkInsertionSort) {
	auto numbers = GetData();
	const auto length = static_cast<int>(numbers.size());

	auto start = std::chrono::high_resolution_clock::now();
	InsertionSort(numbers.data(), length);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	// PrintData(numbers.data(), length, "InsertionSort");
	printf("Test took %f seconds\n", diff.count());
}

/// ---------------------------------------------------------------------------
/// @subsection QuickSort
/// ---------------------------------------------------------------------------

int Partition(int *A, int p, int r) {
	auto x = A[r];
	auto i = p - 1;
	for (auto j = p; j < r; j++) {
		if (A[j] <= x) {
			i = i + 1;
			std::swap(A[i], A[j]);
		}
	}
	std::swap(A[i + 1], A[r]);
	return i + 1;
}

void QuickSort(int *A, int p, int r) {
	if (p < r) {
		auto q = Partition(A, p, r);
		QuickSort(A, p, q - 1);
		QuickSort(A, q + 1, r);
	}
}

TEST(BenchmarkTestSuite, BenchmarkQuickSort) {
	auto numbers = GetData();
	const auto length = static_cast<int>(numbers.size());

	auto start = std::chrono::high_resolution_clock::now();
	QuickSort(numbers.data(), 0, length - 1);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	// PrintData(numbers.data(), length, "QuickSort");
	printf("Test took %f seconds\n", diff.count());
}
