# C++ Intermediate KS

A C++ Intermediate Knowledge Sharing experience.

C++ is one of the most popular programming languages in the world. It is used for everything, from systems-level programming to mobile app development, and is a solid foundation for every programmer's skill set. Let's discuss essentials from legacy c++98/03 to modern c++11, and best practices.

---
## DISCLAIMER
> This project is under constructions.
---

## Download

Download the zip archive and extract the files or clone the project:

```bash
  git clone --recurse-submodules hgit@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git
```

### Build Requirements

* [g++](https://gcc.gnu.org/) (4.8 or higher)
* [cmake](https://cmake.org/) (3.20.0 or higher)
* * [make](https://www.gnu.org/software/make/) (4.2.1 or higher)


## Examples Projects

The examples folder contains two example libraries which can be used to explore C++ code with efficiency and flexibility in mind.

### Effective Modern C++

* Effective Modern C++ is a book from Scott Meyes that describes...  (read the title).
* You can buy the book at [Amazon)](https://www.amazon.es/Effective-Modern-Specific-Ways-Improve/dp/1491903996)

## External Projects

At the moment, the external folder contains the Google Test Framework only, which is added to cmake automatically.

## Authors

- [Pedro André Oliveira](https://www.linkedin.com/in/pedroandreoliveira/)

## Contributing

Contributions are always welcome!

## Related Knowledge Sharings

* [C++ Test-Driven Development](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git)
* [C++ Design Patterns KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git)
* [C++ Clean Architecture KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-clean-architecture.git)

* [C++ Fundamentals KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git)
* [C++ Intermediate KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-intermediate.git)
* [C++ Advanced KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git)
* [C++ Mastery KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git)

## References

* [Effective C++](https://www.amazon.es/Effective-Specific-Programs-Professional-Computing/dp/0321334876)
* [Effective STL](https://www.amazon.es/Effective-STL-Specific-Professional-Computing/dp/0201749629)
* [More Effective C++](https://www.amazon.es/More-Effective-Programs-Professional-Computing/dp/020163371X)
* [Effective Modern C++)](https://www.amazon.es/Effective-Modern-Specific-Ways-Improve/dp/1491903996)

### YouTube

* [Cᐩᐩ Weekly With Jason Turner](https://www.youtube.com/user/lefticus1)
* [CppCon](https://www.youtube.com/user/CppCon)
* [MeetingCpp](https://www.youtube.com/c/MeetingCPP)
