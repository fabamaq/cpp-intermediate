#!/usr/bin/env bash

set -e

# Color
RED='\033[0;31m'
BLUE='\033[1;94m'
WHITE='\033[0m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'

function LogDebug() {
  echo -e "${BLUE}[$1][DEBUG] $2${WHITE}"
}
function LogInfo() {
  echo -e "${GREEN}[INFO] $2${WHITE}"
}
function LogWarn() {
  echo -e "${YELLOW}[WARNING] $2${WHITE}"
}
function LogError() {
  echo -e "${RED}[ERROR] $2${WHITE}"
}

export CMAKE_LOG_LEVEL_TRACE=2
export CMAKE_LOG_LEVEL_DEBUG=3
export CMAKE_LOG_LEVEL_INFO=4
export CMAKE_LOG_LEVEL_WARNING=5
export CMAKE_LOG_LEVEL_ERROR=6
export CMAKE_LOG_LEVEL_FATAL=7
