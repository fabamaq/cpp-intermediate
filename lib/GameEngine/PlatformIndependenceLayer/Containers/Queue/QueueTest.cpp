#include "gtest/gtest.h"

#include "Queue.hpp"

TEST(QueueTestSuite, TestQueue_DefaultQueueIsEmpty) {
	Queue<int, 10> q;
	EXPECT_TRUE(q.Empty());
}

TEST(QueueTestSuite, TestQueue_PopAnEmptyQueueThrowsUnderflowRuntimeError) {
	Queue<int, 10> q;
	EXPECT_THROW(q.Dequeue(), std::runtime_error);
}

TEST(QueueTestSuite, TestQueue_PushIntoAFullQueueThrowsOverflowRuntimeError) {
	Queue<int, 2> q;
	q.Enqueue(1);
	q.Enqueue(2);
	EXPECT_THROW(q.Enqueue(3), std::runtime_error);
}

TEST(QueueTestSuite, TestQueue) {
	Queue<int, 5> q;
	q.Enqueue(4);
	EXPECT_EQ(4, q.Front());
	EXPECT_EQ(4, q.Back());
	EXPECT_EQ(1U, q.Size());
	q.Enqueue(1);
	EXPECT_EQ(4, q.Front());
	EXPECT_EQ(1, q.Back());
	EXPECT_EQ(2U, q.Size());
	q.Enqueue(3);
	EXPECT_EQ(4, q.Front());
	EXPECT_EQ(3, q.Back());
	EXPECT_EQ(3U, q.Size());
	q.Dequeue();
	EXPECT_EQ(1, q.Front());
	EXPECT_EQ(3, q.Back());
	EXPECT_EQ(2U, q.Size());
	q.Enqueue(8);
	EXPECT_EQ(1, q.Front());
	EXPECT_EQ(8, q.Back());
	EXPECT_EQ(3U, q.Size());
	q.Dequeue();
	EXPECT_EQ(3, q.Front());
	EXPECT_EQ(8, q.Back());
	EXPECT_EQ(2U, q.Size());
	q.Enqueue(7);
	EXPECT_EQ(3, q.Front());
	EXPECT_EQ(7, q.Back());
	EXPECT_EQ(3U, q.Size());
	q.Enqueue(6);
	EXPECT_EQ(3, q.Front());
	EXPECT_EQ(6, q.Back());
	EXPECT_EQ(4U, q.Size());
}

#ifndef NDEBUG
TEST(QueueTestSuite, Exercise10_1_3) {
	Queue<int, 5> q;
	EXPECT_EQ("[]", q.ToString());
	q.Enqueue(4);
	EXPECT_EQ("[4]", q.ToString());
	q.Enqueue(1);
	EXPECT_EQ("[4,1]", q.ToString());
	q.Enqueue(3);
	EXPECT_EQ("[4,1,3]", q.ToString());
	q.Dequeue();
	EXPECT_EQ("[1,3]", q.ToString());
	q.Enqueue(8);
	EXPECT_EQ("[1,3,8]", q.ToString());
	q.Dequeue();
	EXPECT_EQ("[3,8]", q.ToString());
	q.Enqueue(7);
	EXPECT_EQ("[3,8,7]", q.ToString());
	q.Enqueue(6);
	EXPECT_EQ("[3,8,7,6]", q.ToString());
}
#endif
