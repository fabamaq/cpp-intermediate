#pragma once

#include <cstddef>

template <typename T = int, std::size_t N = 32> class Queue {
	const int qLength = N - 1;

  public: // Member types
	using value_type = T;
	using size_type = std::size_t;
	using reference = T &;
	using const_reference = const T &;

  public: // Member functions
	Queue() = default;
	~Queue() = default;

	// Element access
	reference Front() {
		ThrowIfEmpty();
		return mData[mHead];
	}

	const_reference Front() const {
		ThrowIfEmpty();
		return mData[mHead];
	}

	reference Back() {
		ThrowIfEmpty();
		if (mTail == 0)
			return mData[N - 1];
		else
			return mData[mTail - 1];
	}

	const_reference Back() const {
		ThrowIfEmpty();
		if (mTail == 0)
			return mData[N - 1];
		else
			return mData[mTail - 1];
	}

	// Capacity

	bool Empty() const { return mCount == 0; }

	size_type Size() const { return mCount; }

	// Modifiers
	void Enqueue(T x) {
		ThrowIfFull();
		mData[mTail] = x;
		if (mTail == qLength)
			mTail = 0;
		else
			mTail++;
		mCount++;
	}

	T Dequeue() {
		ThrowIfEmpty();
		auto x = mData[mHead];
		if (mHead == qLength)
			mHead = 0;
		else
			mHead++;
		mCount--;
		return x;
	}

  private:
	void ThrowIfFull() {
		if (mCount == N)
			throw std::runtime_error("Overflow exception!");
	}
	void ThrowIfEmpty() {
		if (mCount == 0)
			throw std::runtime_error("Underflow exception!");
	}

  private:
	T mData[N]{};
	int mHead{0};
	int mTail{0};
	size_type mCount{0};

#ifndef NDEBUG
  public:
	std::string ToString() const {
		if (Empty())
			return "[]";

		std::string result{""};
		result.reserve(255);
		result += "[";
		if (mHead < mTail) {
			for (int i = mHead; i < mTail - 1; i++) {
				result += std::to_string(mData[i]) + ",";
			}
			result += std::to_string(mData[mTail - 1]);
			result += "]";
		} else {
			for (auto i = static_cast<size_type>(mHead); i < N - 1; i++) {
				result += std::to_string(mData[i]) + ",";
			}
			if (mTail > 0) {
				result += std::to_string(mData[N - 1]) + ",";
				for (int j = 0; j < mTail - 2; j++) {
					result += std::to_string(mData[j]) + ",";
				}
				result += std::to_string(mData[mTail - 1]);
				result += "]";
			} else {
				result += std::to_string(mData[N - 1]);
				result += "]";
			}
		}

		return result;
	}
#endif
};
