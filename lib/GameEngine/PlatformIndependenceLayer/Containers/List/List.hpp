#pragma once

#include <cstddef>

#include <iterator>
#include <type_traits>

/******************************************************************************
 * @brief
 *
 *****************************************************************************/
struct SortingPolicyBase {};

/******************************************************************************
 * @brief
 *
 * @tparam NodeT
 *****************************************************************************/
template <class NodeT> struct UnsortedListT : public SortingPolicyBase {
	static NodeT *Insert(NodeT *head, NodeT *x) {
		x->next = head;
		return x;
	}
	static NodeT *Minimum(NodeT *head) { return head; }
	static NodeT *Maximum(NodeT *head) {
		auto curr = head;
		auto max = head;
		while (curr->next != nullptr && curr->next != head) {
			if (curr->key > max->key) {
				max = curr;
			}
			curr = curr->next;
		}
		return max;
	}
};

/******************************************************************************
 * @brief
 *
 * @tparam NodeT
 *****************************************************************************/
template <class NodeT> struct SortedListT : public SortingPolicyBase {
	static NodeT *Insert(NodeT *head, NodeT *x) {
		if (head == nullptr) {
			head = x;
			return head;
		}
		auto n = head;
		while (n->next != nullptr && x->key >= n->key) {
			x->next = n->next;
			if (n->next->key > x->key) {
				n->next = x;
			}
			n = x->next;
		}
		if (x->key > n->key) {
			x->next = n->next;
			n->next = x;
		} else {
			x->next = n;
		}
		if (x->key < head->key) {
			head = x;
		}
		return head;
	}
	static NodeT *Minimum(NodeT *head) { return head; }

	static NodeT *Maximum(NodeT *head) {
		auto tail = head;
		while (tail->next != nullptr && tail->next != head) {
			tail = tail->next;
		}
		return tail;
	}
};

/******************************************************************************
 * @brief
 *
 *****************************************************************************/
struct DirectionPolicyBase {};

/******************************************************************************
 * @brief
 *
 * @tparam T
 *****************************************************************************/
template <class T> struct SinglyLinkedT : public DirectionPolicyBase {

	struct Node {
		T key;
		Node *next;

		Node(T k, Node *n = nullptr) : key(k), next(n) {
			// printf("SinglyLinkedT::Node(%d)\n", k);
		}
		// ~Node() { puts("SinglyLinkedT::~Node()"); }
		bool operator==(const Node &other) const { return key == other.key; }
	};

	static Node *Update(Node *) { return nullptr; }

	static Node *Delete(Node *head, Node *x) {
		if (head == x) {
			head = head->next;
			x->next = nullptr;
			return head;
		}
		auto curr = head;
		while (curr->next != nullptr && curr->next != x) {
			curr = curr->next;
		}
		curr->next = x->next;
		x->next = nullptr;
		return head;
	}

	Node *Predecessor(Node *head, Node *x) {
		auto curr = head;
		while (curr != nullptr && curr->next != nullptr &&
			   curr->next->key != x->key) {
			curr = curr->next;
		}
		return curr;
	}
};

/******************************************************************************
 * @brief
 *
 * @tparam T
 *****************************************************************************/
template <class T> struct DoublyLinkedT : public DirectionPolicyBase {
	struct Node {
		T key;
		Node *next;
		Node *prev;
		Node(T k, Node *n = nullptr, Node *p = nullptr)
			: key(k), next(n), prev(p) {
			// printf("DoublyLinkedT::Node(%d)\n", k);
		}
		// ~Node() { puts("DoublyLinkedT::~Node()"); }
		bool operator==(const Node &other) const { return key == other.key; }
	};

	static Node *Update(Node *head) {
		auto curr = head;
		// we stop when tail points to null or to head
		while (curr->next != nullptr && curr->next != head) {
			curr->next->prev = curr;
			curr = curr->next;
		}
		return (curr != head ? curr->next : nullptr);
	}

	static Node *Delete(Node *head, Node *x) {
		if (x->prev != nullptr) {
			x->prev->next = x->next;
		} else {
			head = x->next;
		}
		if (x->next != nullptr) {
			x->next->prev = x->prev;
		}
		return head;
	}

	Node *Predecessor(Node *, Node *x) { return x->prev; }
};

/******************************************************************************
 * @brief
 *
 *****************************************************************************/
struct EdgeLinkingPolicyBase {};

/******************************************************************************
 * @brief
 *
 * @tparam lass
 * @tparam class
 *****************************************************************************/
template <class, class = void> struct has_prev_member : std::false_type {};

/******************************************************************************
 * @brief
 *
 * @tparam T
 *****************************************************************************/
template <class T>
struct has_prev_member<T, std::void_t<decltype(std::declval<T>().prev)>>
	: std::true_type {};

template <typename T>
constexpr bool has_prev_member_v = has_prev_member<T>::value;

/******************************************************************************
 * @brief
 *
 * @tparam NodeT
 *****************************************************************************/
template <class NodeT, typename = std::enable_if_t<has_prev_member_v<NodeT>>,
		  int>
void ConnectEdges(NodeT *head, NodeT *tail) {
	if (head != nullptr && tail != nullptr) {
		head->prev = tail;
		tail->next = head;
	}
}

/******************************************************************************
 * @brief
 *
 * @tparam NodeT
 *****************************************************************************/
template <class NodeT> void ConnectEdges(NodeT *head, NodeT *tail) {
	if (head != nullptr && tail != nullptr) {
		tail->next = head;
	}
}

/******************************************************************************
 * @brief
 *
 * @tparam NodeT
 *****************************************************************************/
template <class NodeT> struct CircularListT : public EdgeLinkingPolicyBase {
	static void Update(NodeT *head, NodeT *tail) {
		ConnectEdges<NodeT>(head, tail);
	}
};

/******************************************************************************
 * @brief
 *
 * @tparam DirectionPolicy
 *****************************************************************************/
template <class DirectionPolicy>
struct NonCircularListT : public EdgeLinkingPolicyBase {
	using NodeT = typename DirectionPolicy::Node;
	static void Update(NodeT *, NodeT *) {}
};

/******************************************************************************
 * @brief a Policy-based List
 *
 * @tparam T The type of the elements.
 * @tparam SortingPolicy
 * @tparam DirectionPolicy
 * @tparam EdgeLinkingPolicy
 * @tparam Allocator An allocator that is used to acquire/release memory and to
 * construct/destroy the elements in that memory.
 *****************************************************************************/
template <class T, template <class> class SortingPolicy = UnsortedListT,
		  template <class> class DirectionPolicy = SinglyLinkedT,
		  template <class> class EdgeLinkingPolicy = NonCircularListT,
		  class Allocator = std::allocator<T>>

class List : public DirectionPolicy<T> {
  public:
	/// =======================================================================
	/// @section Member types
	/// =======================================================================
	using value_type = T;
	using allocator_type = Allocator;
	using size_type = std::size_t;
	using difference_type = std::ptrdiff_t;
	using reference = value_type &;
	using const_reference = const value_type &;
	using pointer = typename std::allocator_traits<Allocator>::pointer;
	using const_pointer =
		typename std::allocator_traits<Allocator>::const_pointer;

	// using iterator = DirectionPolicy<T>::iterator;
	// using const_iterator = DirectionPolicy<T>::const_iterator;
	// using reverse_iterator = std::reverse_iterator<iterator>;
	// using const_reverse_iterator = std::reverse_iterator<const_iterator>;

	using Node = typename DirectionPolicy<T>::Node;

	/// =======================================================================
	/// @section Member functions
	/// =======================================================================

	/**
	 * @brief Default constructor. Constructs an empty container with a
	 * default-constructed allocator.
	 */
	explicit List() {}

	/**
	 * @brief Constructs an empty container with the given allocator `alloc`.
	 *
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	explicit List(const Allocator &alloc);

	/**
	 * @brief Constructs the container with count copies of elements with value
	 * value.
	 *
	 * @param count the size of the container
	 * @param value the value to initialize elements of the container with
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	explicit List(size_type count, const T &value,
				  const Allocator &alloc = Allocator());

	/**
	 * @brief Constructs the container with count default-inserted instances of
	 * T. No copies are made.
	 *
	 * @param count the size of the container
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	explicit List(size_type count, const Allocator &alloc = Allocator());

	/**
	 * @brief Constructs the container with the contents of the range [first,
	 * last).
	 *
	 * @param first the beginning of the range to copy the elements from
	 * @param last the end of the range to copy the elements from
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	template <class InputIt>
	List(InputIt first, InputIt last, const Allocator &alloc = Allocator());

	/**
	 * @brief Copy constructor. Constructs the container with the copy of the
	 * contents of other.
	 *
	 * @param other another container to be used as source to initialize the
	 * elements of the container with
	 */
	List(const List &other);

	/**
	 * @brief Constructs the container with the copy of the contents of other,
	 * using alloc as the allocator.
	 *
	 * @param other another container to be used as source to initialize the
	 * elements of the container with
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	List(const List &other, const Allocator &alloc);

	/**
	 * @brief Move constructor. Constructs the container with the contents of
	 * other using move semantics. Allocator is obtained by move-construction
	 * from the allocator belonging to other.
	 *
	 * @param other another container to be used as source to initialize the
	 * elements of the container with
	 */
	List(List &&other);

	/**
	 * @brief Allocator-extended move constructor. Using alloc as the allocator
	 * for the new container, moving the contents from other; if alloc !=
	 * other.get_allocator(), this results in an element-wise move.
	 *
	 * @param other another container to be used as source to initialize the
	 * elements of the container with
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	List(List &&other, const Allocator &alloc);

	/**
	 * @brief Constructs the container with the contents of the initializer list
	 * init.
	 *
	 * @param init initializer list to initialize the elements of the container
	 * with
	 * @param alloc allocator to use for all memory allocations of this
	 * container
	 */
	List(std::initializer_list<T> init, const Allocator &alloc = Allocator());

	/**
	 * @brief Destructs the list. The destructors of the elements are called and
	 * the used storage is deallocated. Note, that if the elements are pointers,
	 * the pointed-to objects are not destroyed.
	 *
	 */
	~List() = default;

	/**
	 * @brief Copy assignment operator. Replaces the contents with a copy of the
	 * contents of other.
	 *
	 * @param other another container to use as data source
	 * @return *this
	 */
	List &operator=(const List &other);

	/**
	 * @brief Move assignment operator. Replaces the contents with those of
	 * other using move semantics (i.e. the data in other is moved from other
	 * into this container). other is in a valid but unspecified state
	 * afterwards.
	 *
	 * @param other another container to use as data source
	 * @return *this
	 */
	List &operator=(List &&other) noexcept(
		std::allocator_traits<Allocator>::is_always_equal::value);

	/**
	 * @brief Replaces the contents with those identified by initializer list
	 * ilist.
	 *
	 * @param ilist initializer list to use as data source
	 * @return *this
	 */
	List &operator=(std::initializer_list<T> ilist);

	/**
	 * @brief
	 *
	 * @param count
	 * @param value
	 */
	void Assign(size_type count, const T &value);

	/**
	 * @brief
	 *
	 * @param first
	 * @param last
	 */
	template <class InputIt> void Assign(InputIt first, InputIt last);

	/**
	 * @brief
	 *
	 * @param ilist
	 */
	void Assign(std::initializer_list<T> ilist);

	/**
	 * @brief Returns the allocator associated with the container.
	 *
	 * @return The associated allocator.
	 */
	allocator_type get_allocator() const noexcept;

	/// -----------------------------------------------------------------------
	/// @subsection Element access
	/// -----------------------------------------------------------------------

	/**
	 * @brief Returns a reference to the first element in the container. Calling
	 * front on an empty container is undefined.
	 *
	 * @return reference to the first element
	 */
	reference Front();

	/**
	 * @brief Returns a reference to the first element in the container. Calling
	 * front on an empty container is undefined.
	 *
	 * @return reference to the first element
	 */
	const_reference Front() const;

	/**
	 * @brief Returns a reference to the last element in the container. Calling
	 * back on an empty container causes undefined behavior.
	 *
	 * @return reference to the last element
	 */
	reference Back();

	/**
	 * @brief Returns a reference to the last element in the container. Calling
	 * back on an empty container causes undefined behavior.
	 *
	 * @return reference to the last element
	 */
	const_reference Back() const;

	/// -----------------------------------------------------------------------
	/// @subsection Iterators
	/// -----------------------------------------------------------------------

	/**
	 * @brief Returns a pointer to the first element of the list. If the list is
	 * empty, the returned iterator will be nullptr.
	 *
	 * @return the first element.
	 */
	Node *Begin() noexcept { return mHead; }

	/**
	 * @brief Returns a pointer to the first element of the list. If the list is
	 * empty, the returned iterator will be nullptr.
	 *
	 * @return the first element.
	 */
	const Node *Begin() const noexcept { return mHead; }

	/**
	 * @brief Returns a pointer to the first element of the list. If the list is
	 * empty, the returned iterator will be nullptr.
	 *
	 * @return the first element.
	 */
	const Node *CBegin() const noexcept { return mHead; }

	/**
	 * @brief Returns a pointer to the last element of the list. If the list is
	 * empty, the returned iterator will be nullptr.
	 *
	 * @return the last element.
	 */
	Node *End() noexcept { return DirectionPolicy<T>::Tail(mHead); }

	/**
	 * @brief Returns a pointer to the last element of the list. If the list is
	 * empty, the returned iterator will be nullptr.
	 *
	 * @return the last element.
	 */
	const Node *End() const noexcept { return DirectionPolicy<T>::Tail(mHead); }

	/**
	 * @brief Returns a pointer to the last element of the list. If the list is
	 * empty, the returned iterator will be nullptr.
	 *
	 * @return the last element.
	 */
	const Node *CEnd() const noexcept {
		return DirectionPolicy<T>::Tail(mHead);
	}

	/// -----------------------------------------------------------------------
	/// @subsection Capacity
	/// -----------------------------------------------------------------------

	/**
	 * @brief Checks if the container has no elements, i.e. whether begin() ==
	 * end().
	 *
	 * @return true if the container is empty, false otherwise
	 */
	[[nodiscard]] bool Empty() const noexcept;

	/**
	 * @brief Returns the number of elements in the container, i.e.
	 * std::distance(begin(), end()).
	 *
	 * @return The number of elements in the container.
	 */
	size_type Size() const noexcept;

	/**
	 * @brief Returns the maximum number of elements the container is able to
	 * hold due to system or library implementation limitations, i.e.
	 * std::distance(begin(), end()) for the largest container.
	 *
	 * @return Maximum number of elements.
	 */
	size_type MaxSize() const noexcept;

	/// -----------------------------------------------------------------------
	/// @subsection Modifiers
	/// -----------------------------------------------------------------------

	/**
	 * @brief Erases all elements from the container. After this call, Size()
	 returns zero.
	 */
	void Clear() noexcept;

	/// @todo insert
	/// @todo emplace
	/// @todo erase
	/// @todo push_back
	/// @todo emplace_back
	/// @todo pop_back
	/// @todo push_front
	/// @todo emplace_front
	/// @todo pop_front
	/// @todo resize
	/// @todo swap

	/// -----------------------------------------------------------------------
	/// @subsection Operations
	/// -----------------------------------------------------------------------

	/// @todo merge
	/// @todo splice
	/// @todo remove
	/// @todo remove_if
	/// @todo reverse
	/// @todo unique
	/// @todo sort

  public:
	/**
	 * @brief
	 *
	 * @param k
	 * @return Node*
	 */
	Node *Search(T k) {
		auto x = mHead;
		while (x != nullptr && x->key != k) {
			x = x->next;
		}
		return x;
	}

	/**
	 * @brief
	 *
	 * @param x
	 */
	void Insert(Node *x) {
		mHead = SortingPolicy<Node>::Insert(mHead, x);
		auto tail = DirectionPolicy<T>::Update(mHead);
		EdgeLinkingPolicy<Node>::Update(mHead, tail);
		mCount++;
	}

	/**
	 * @brief
	 *
	 * @param x
	 */
	void Delete(Node *x) {
		mHead = DirectionPolicy<T>::Delete(mHead, x);
		mCount--;
	}

	/**
	 * @brief
	 *
	 * @return Node*
	 */
	Node *Minimum() { return SortingPolicy<Node>::Minimum(mHead); }

	/**
	 * @brief
	 *
	 * @return Node*
	 */
	Node *Maximum() { return SortingPolicy<Node>::Maximum(mHead); }

	/**
	 * @brief
	 *
	 * @param x
	 * @return Node*
	 */
	Node *Successor(Node *x) { return x->next; }

	/**
	 * @brief A query that, given an element x whose key is from a totally
	 * ordered set S, returns a pointer to the next smaller element in S, or NIL
	 * if x is the minimum element
	 *
	 * @param x an element whose key is from a totally ordered set S
	 * @return Node* pointer to the next smaller element in S
	 */
	Node *Predecessor(Node *x) {
		return DirectionPolicy<T>::Predecessor(mHead, x);
	}

	/**
	 * @brief Get the Head object
	 *
	 * @return Node*
	 */
	Node *GetHead() { return mHead; }

	/**
	 * @brief
	 *
	 * @return std::size_t
	 */
	std::size_t Size() { return mCount; }

  private:
	Node *mHead{nullptr};
	std::size_t mCount{0};
};
