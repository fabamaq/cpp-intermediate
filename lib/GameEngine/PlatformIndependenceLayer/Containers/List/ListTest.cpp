#include "gtest/gtest.h"

#include "List.hpp"

/**
 * UnsortedListT,
 * SinglyLinkedT,
 * NonCircularListT
 */
TEST(ListTestSuite, Test_Unsorted_SinglyLinked_NonCircular_List) {
	List<int, UnsortedListT, SinglyLinkedT, NonCircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);
	EXPECT_EQ(1U, theList.Size());

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);
	EXPECT_EQ(2U, theList.Size());

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);
	EXPECT_EQ(3U, theList.Size());

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);
	EXPECT_EQ(4U, theList.Size());

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);
	EXPECT_EQ(5U, theList.Size());

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21
	EXPECT_EQ(4U, theList.Size());

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode4);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * SortedListT,
 * SinglyLinkedT,
 * NonCircularListT
 */
TEST(ListTestSuite, Test_Sorted_SinglyLinked_NonCircular_List) {
	List<int, SortedListT, SinglyLinkedT, NonCircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode1);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * UnsortedListT,
 * DoublyLinkedT,
 * NonCircularListT
 */
TEST(ListTestSuite, Test_Unsorted_DoublyLinked_NonCircular_List) {
	List<int, UnsortedListT, DoublyLinkedT, NonCircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode4);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * SortedListT,
 * DoublyLinkedT,
 * NonCircularListT
 */
TEST(ListTestSuite, Test_Sorted_DoublyLinked_NonCircular_List) {
	List<int, SortedListT, DoublyLinkedT, NonCircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode1);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * UnsortedListT,
 * SinglyLinkedT,
 * CircularListT
 */
TEST(ListTestSuite, Test_Unsorted_SinglyLinked_Circular_List) {
	List<int, UnsortedListT, SinglyLinkedT, CircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode4);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * SortedListT,
 * SinglyLinkedT,
 * CircularListT
 */
TEST(ListTestSuite, Test_Sorted_SinglyLinked_Circular_List) {
	List<int, SortedListT, SinglyLinkedT, CircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode1);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * UnsortedListT,
 * DoublyLinkedT,
 * CircularListT
 */
TEST(ListTestSuite, Test_Unsorted_DoublyLinked_Circular_List) {
	List<int, UnsortedListT, DoublyLinkedT, CircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode4);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}

/**
 * SortedListT,
 * DoublyLinkedT,
 * CircularListT
 */
TEST(ListTestSuite, Test_Sorted_DoublyLinked_Circular_List) {
	List<int, SortedListT, DoublyLinkedT, CircularListT> theList;
	using ListT = decltype(theList);

	// ------------------------------------------------------------------------
	// Test Insertion
	ListT::Node newNode1{7};
	theList.Insert(&newNode1);

	ListT::Node newNode2{42};
	theList.Insert(&newNode2);

	ListT::Node newNode3{21};
	theList.Insert(&newNode3);

	ListT::Node newNode4{1};
	theList.Insert(&newNode4);

	ListT::Node newNode5{21};
	theList.Insert(&newNode5);

	// ------------------------------------------------------------------------
	// Test Deletion
	theList.Delete(&newNode5); // 21

	// ------------------------------------------------------------------------
	// Test Successor
	auto successor = theList.Successor(&newNode3);
	EXPECT_EQ(successor, &newNode2); // 21 -> 42

	// ------------------------------------------------------------------------
	// Test Predecessor
	auto predecessor = theList.Predecessor(&newNode3);
	EXPECT_EQ(predecessor, &newNode1);

	// ------------------------------------------------------------------------
	// Test Maximum
	auto minimum = theList.Minimum();
	EXPECT_EQ(minimum, &newNode4);

	// ------------------------------------------------------------------------
	// Test Minimum
	auto maximum = theList.Maximum();
	EXPECT_EQ(maximum, &newNode2);
}
