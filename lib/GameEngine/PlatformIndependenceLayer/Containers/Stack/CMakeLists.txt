file(GLOB containers_FOLDERS RELATIVE ${CMAKE_CURRENT_LIST_DIR} ${CMAKE_CURRENT_LIST_DIR}/*)
foreach(folder ${containers_FOLDERS})
  add_subdirectory(${folder})
endforeach()
