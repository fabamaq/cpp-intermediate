#pragma once

#include <string>

template <typename T, std::size_t N = 32> class Stack {
  public: // Member types
	using value_type = T;
	using size_type = std::size_t;
	using reference = T &;
	using const_reference = const T &;

  public: // Member functions
	Stack() = default;
	~Stack() = default;

	// Element access
	reference Top() { return mData[mCount]; }
	const_reference Top() const { return mData[mCount]; }

	// Capacity
	size_type Size() const { return mCount; }
	bool Empty() const { return mCount == 0; }

	// Modifiers
	void Push(const value_type &x) {
		ThrowIfFull();
		mData[mCount] = x;
		mCount++;
	}

	void Push(value_type &&x) {
		ThrowIfFull();
		mData[mCount] = std::move(x);
		mCount++;
	}

	void Pop() {
		ThrowIfEmpty();
		mCount--;
	}

  private:
	void ThrowIfFull() {
		if (mCount == N) {
			throw std::runtime_error("Overflow exception!");
		}
	}

	void ThrowIfEmpty() {
		if (mCount == 0) {
			throw std::runtime_error("Underflow exception!");
		}
	}

  private:
	T mData[N]{};
	size_type mCount{0};

#ifndef NDEBUG
  public:
	std::string ToString() const {
		if (Empty())
			return "[]";

		std::string result;
		result += "[";
		for (auto i = 0U; i < mCount - 1; i++) {
			result += std::to_string(mData[i]) + ",";
		}
		result += std::to_string(mData[mCount - 1]);
		result += "]";
		return result;
	}
#endif
};
