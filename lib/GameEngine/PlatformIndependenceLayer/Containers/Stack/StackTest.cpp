#include "gtest/gtest.h"

#include "Stack.hpp"

TEST(StackTestSuite, TestStack_DefaultStackIsEmpty) {
	Stack<int, 10> s;
	EXPECT_TRUE(s.Empty());
}

TEST(StackTestSuite, TestStack_PopAnEmptyStackThrowsUnderflowRuntimeError) {
	Stack<int, 10> s;
	EXPECT_THROW(s.Pop(), std::runtime_error);
}

TEST(StackTestSuite,
	 TestStack_PopANonEmptyStackDoesNotThrowsUnderflowRuntimeError) {
	Stack<int, 10> s;
	s.Push(1);
	EXPECT_NO_THROW(s.Pop());
}

TEST(StackTestSuite, TestStack_PushIntoAFullStackThrowsOverflowRuntimeError) {
	Stack<int, 2> s;
	s.Push(1);
	s.Push(2);
	EXPECT_THROW(s.Push(3);, std::runtime_error);
}

#ifndef NDEBUG
TEST(StackTestSuite, Exercise10_1_1) {
	Stack<int, 5> s;
	EXPECT_EQ("[]", s.ToString());
	s.Push(4);
	EXPECT_EQ("[4]", s.ToString());
	s.Push(1);
	EXPECT_EQ("[4,1]", s.ToString());
	s.Push(3);
	EXPECT_EQ("[4,1,3]", s.ToString());
	s.Pop();
	EXPECT_EQ("[4,1]", s.ToString());
	s.Push(8);
	EXPECT_EQ("[4,1,8]", s.ToString());
	s.Pop();
	EXPECT_EQ("[4,1]", s.ToString());
}
#endif
