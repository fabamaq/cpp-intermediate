#include "gtest/gtest.h"

#include <iostream>
#include <limits>
#include <numeric>

/******************************************************************************
 * Helpers
 *****************************************************************************/
void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

/******************************************************************************
 * The divide-and-conquer approach
 *****************************************************************************/
void Merge(int *A, int p, int q, int r) {
	auto n1 = static_cast<unsigned long>(q - p + 1);
	auto n2 = static_cast<unsigned long>(r - q);
	int *L = new int[n1 + 1];
	int *R = new int[n2 + 1];
	for (unsigned long i = 0; i < n1; i++)
		L[i] = A[static_cast<unsigned long>(p) + i - 1];
	for (unsigned long j = 0; j < n2; j++)
		R[j] = A[static_cast<unsigned long>(q) + j];
	L[n1] = std::numeric_limits<int>::max();
	R[n2] = std::numeric_limits<int>::max();
	int i = 0;
	int j = 0;
	for (int k = p - 1; k < r; k++) {
		if (L[i] <= R[j]) {
			A[k] = L[i];
			i = i + 1;
		} else {
			A[k] = R[j];
			j = j + 1;
		}
	};
	delete[] L;
	delete[] R;
}

// [ r1, r2, ..., r n/2, ..., rn]
void MergeSort(int *A, int p, int r) {
	if (p < r) {
		int q = (p + r) / 2;
		MergeSort(A, p, q);
		MergeSort(A, q + 1, r);
		Merge(A, p, q, r);
	}
}

TEST(MergeSortTestSuite, TestMergeSort) {
	int numbers[]{5, 2, 4, 7, 1, 3, 2, 6};
	constexpr auto numbers_size = sizeof(numbers) / sizeof(int);
	MergeSort(numbers, 1, numbers_size);
	int expected[]{1, 2, 2, 3, 4, 5, 6, 7};
	for (auto i = 0U; i < numbers_size; i++) {
		ASSERT_EQ(numbers[i], expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}
