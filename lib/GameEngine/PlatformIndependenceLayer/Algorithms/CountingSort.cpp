#include "gtest/gtest.h"

#include <iostream>

/******************************************************************************
 * Helpers
 *****************************************************************************/
void PrintData(int *data, int size, const char *name) {
	std::cout << name << ": [ ";
	for (int i = 0; i < size; i++) {
		std::cout << data[i] << " ";
	}
	std::cout << "]\n";
}

/******************************************************************************
 * @brief Counting Sort assumes each of the n elements is in the range [0, k]
 *****************************************************************************/
void CountingSort(int *A, int length, int *B, int k) {
	int *C = new int[k + 1];
	for (int i = 0; i <= k; i++) {
		C[i] = 0;
	}
	for (int j = 0; j < length; j++) {
		C[A[j]] = C[A[j]] + 1;
	}
	// C[i] now contains the number of elements equal to i.
	for (int i = 1; i <= k; i++) {
		C[i] = C[i] + C[i - 1];
	}
	// C[i] now contains the number of elements less than or equal to i.
	for (int j = length - 1; j >= 0; j--) {
		B[C[A[j]] - 1] = A[j];
		C[A[j]] = C[A[j]] - 1;
	}
	delete[] C;
}

TEST(CountingSortTestSuite, TestCountingSort) {
	int A[]{2, 5, 3, 0, 2, 3, 0, 3};
	constexpr auto length = sizeof(A) / sizeof(int);
	int B[length]{0};
	int k = 5;
	CountingSort(A, length, B, k);
	int Expected[]{0, 0, 2, 2, 3, 3, 3, 5};
	for (auto i = 0U; i < length; i++) {
		ASSERT_EQ(B[i], Expected[i]) << "Mismatch at index " << i;
	}
	SUCCEED();
}
