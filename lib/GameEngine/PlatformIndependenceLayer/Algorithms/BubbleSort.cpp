#include "BubbleSort.hpp"

#include <numeric>

void BubbleSort(int *A, int length) {
	for (int i = 0; i < length - 1 - 1; i++) {
		for (int j = length - 1; j >= i + 1; j--) {
			if (A[j] < A[j - 1])
				std::swap(A[j], A[j - 1]);
		}
	};
}
