#pragma once

/// ===========================================================================
/// @section Utilities library
/// ===========================================================================
#include <any>		// std::any class
#include <optional> // std::optional class template
#include <variant>	// std::variant class template

/// ---------------------------------------------------------------------------
/// @subsection Dynamic memory management
/// ---------------------------------------------------------------------------
#include <memory_resource> // Polymorphic allocators and memory resources

/// ===========================================================================
/// @section Strings library
/// ===========================================================================
#include <charconv>	   // std::to_chars and std::from_chars
#include <string_view> // std::basic_string_view class template

/// ===========================================================================
/// @section Algorithms library
/// ===========================================================================
#include <execution> // Predefined execution policies for parallel versions of the algorithms

/// ===========================================================================
/// @section Filesystem library
/// ===========================================================================
#include <filesystem> // std::path class and supporting functions
